import numpy as np
from activations import *
from costs import *
import random
import matplotlib.pyplot as plt

class NeuralNetwork(object):
    def __init__(self,neurons:list,X:list,Y:list,activation):

        self.n_x = X.shape[1]
        self.n_y = Y.shape[1]
        self.layers = neurons.shape[0]
        self.activation = self.activation_init(activation)
        self.derivative = self.derivative_init(activation)
        self.parameters = self.w_b_initialize(neurons)
        self.grads = {}
        self.cache={}
        self.cache['Z'+str(self.layers)] = np.zeros(Y.shape)
    
    def activation_init(self,activation):
        ''' initializing the activation function
        '''
        if activation == 'sigmoid':
            return sigmoid
        elif activation == 'relu':
            return relu
        elif activation == 'tanh':
            return tanh

    def derivative_init(self,activation):
        ''' initializing the derivative of activation function
        '''
        if self.activation == sigmoid:
            return derivative_sigmoid
        elif self.activation == relu:
            return derivative_relu
        elif self.activation == tanh:
            return derivative_tanh

    def weights_reg(self,units):
        ''' 'He' initialization for ReLU, Xavier for tanh
        '''
        if self.activation == sigmoid:
            return 1
        elif self.activation == relu:
            return np.sqrt(2./units)
        elif self.activation == tanh:
            return np.sqrt(1./units)
            
    def w_b_initialize(self,neurons,seed=1):
        ''' weights and bias initialization
        '''
        np.random.seed(seed)

        parameters = {}
        parameters['W0'] = np.random.randn(self.n_x,neurons[0])*self.weights_reg(self.n_x)
        parameters['b0'] = np.zeros((1,neurons[0]))

        for unit in range(1,self.layers):
            parameters['W'+str(unit)] = np.random.randn(neurons[unit-1],neurons[unit])*self.weights_reg(neurons[unit-1])
            parameters['b'+str(unit)] = np.zeros((1,neurons[unit]))
        
        parameters['W'+str(self.layers)] = np.random.randn(neurons[-1],1)*self.weights_reg(neurons[-1])
        parameters['b'+str(self.layers)] = np.zeros((1,self.n_y))

        return parameters

    def forward_propagation(self,X):
        ''' forward propagation step
        '''

        parameters=self.parameters
        cache={}


        cache['Z0'] = np.dot(X,parameters['W0'])+parameters['b0']
        cache['A0'] = relu(cache['Z0'])
        
        for i in range(1,self.layers):
            cache['Z'+str(i)] = np.dot(cache['A'+str(i-1)],parameters['W'+str(i)])+parameters['b'+str(i)]
            cache['A'+str(i)] = relu(cache['Z'+str(i)])
        
        cache['Z'+str(self.layers)] = np.dot(cache['A'+str(self.layers-1)],parameters['W'+str(self.layers)])+parameters['b'+str(self.layers)]
        cache['A'+str(self.layers)] = self.activation(cache['Z'+str(self.layers)])

        return cache

    def backward_propagation(self,X,y):
        '''backward propagation step
        '''
        grads = self.grads
        cache = self.cache
        parameters = self.parameters

        grads['dZ'+str(self.layers)] = (cache['A'+str(self.layers)]-y)*self.derivative(cache['A'+str(self.layers)])
        grads['dW'+str(self.layers)] = (1/self.n_x)*np.dot(cache['A'+str(self.layers-1)].T,grads['dZ'+str(self.layers)])
        grads['db'+str(self.layers)] = (1/self.n_x)*np.sum(grads['dZ'+str(self.layers)],axis=0,keepdims=True)

        
        for i in range(self.layers-1,0,-1):
            grads['dZ'+str(i)] = np.dot(grads['dZ'+str(i+1)],parameters['W'+str(i+1)].T)*derivative_relu(cache['A'+str(i)])
            grads['dW'+str(i)] = (1/self.n_x)*np.dot(cache['A'+str(i-1)].T,grads['dZ'+str(i)])
            grads['db'+str(i)] = (1/self.n_x)*np.sum(grads['dZ'+str(i)],axis=0,keepdims=True)

        grads['dZ0'] = np.dot(grads['dZ1'],parameters['W1'].T)*derivative_relu(cache['A0'])
        grads['dW0'] = (1/self.n_x)*np.dot(X.T,grads['dZ0'])
        grads['db0'] = (1/self.n_x)*np.sum(grads['dZ0'],axis=0,keepdims=True)

        return grads

    def optimize(self,learning_rate):
        ''' gradient descent
        '''
        parameters = self.parameters
        grads = self.grads

        for i in range(0,self.layers+1):
            parameters['W'+str(i)] = parameters['W'+str(i)] - learning_rate*grads['dW'+str(i)]
            parameters['b'+str(i)] = parameters['b'+str(i)] - learning_rate*grads['db'+str(i)]
        
        return parameters

    def predict(self,X):
        return self.forward_propagation(X)['A'+str(self.layers)]

    def train(self,X, Y, batch_size = 32, learning_rate = 0.01, epochs = 20, cost = 'mse', print_cost = False, cost_plot = False):
        error_curve=[]
        cost_f = rmse

        if cost == 'rmse':
            cost_f = rmse
        elif cost == 'cross_entropy':
            cost_f = cross_entropy
        elif cost == 'mse':
            cost_f = mse

        for epoch in range(epochs):
            for i in range(0,X.shape[0],batch_size):
                X_mini = X[i:batch_size]
                Y_min = Y[i:batch_size]
                self.cache = self.forward_propagation(X)
                self.grads = self.backward_propagation(X,Y)
                self.parameters = self.optimize(learning_rate)
            error=cost_f(Y,self.cache['A'+str(self.layers)])
            print(f'Cost after {epoch} epochs: {error}')
            error_curve.append(error)

        if cost_plot:
            ''' print cost plot
            '''
            func = plt.figure()
            plt.plot(error_curve)
            func.suptitle(f'Cost function: {cost}', fontsize=20)
            plt.xlabel('Epochs: ', fontsize=18)
            plt.ylabel('Cost value: ', fontsize=16)
            plt.show()
