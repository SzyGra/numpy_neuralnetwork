# Numpy_NeuralNetwork
## Implementation of a neural network from scratch with numpy and matplotlib (for cost visualization)
## Setup
The `requirements.txt` list all Python libraries that you need in that neural network implementation.
They will be installed using:
```
pip install -r requirements.txt
```