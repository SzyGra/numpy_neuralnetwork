import numpy as np

def cross_entropy(targets,predictions):
    m = len(targets)
    
    cost = -(1/m)*np.sum(targets*np.log(predictions)+(1-targets)*np.log(1-predictions))

    return cost

def rmse(targets,predictions):
    return np.sqrt(np.mean((predictions-targets)**2))

def mse(targets,predictions):
    return np.sum((predictions - targets)**2)/len(targets)