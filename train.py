from nn_model import NeuralNetwork
import numpy as np
import pickle
def train():
    x = np.array([[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16]])
    y = np.array([[1],[4],[9],[16],[25],[36],[49],[64],[81],[100],[121],[144],[169],[184],[225],[256]])
    neurons = np.array([2,4,2])
    model = NeuralNetwork(neurons,x,y,'relu')
    epochs = 100000
    learning_rate = 0.0000000001
    try:
        model.train(x,y,learning_rate=learning_rate,batch_size = 4,epochs = epochs,cost = 'rmse',print_cost=True, cost_plot = True)
    except KeyboardInterrupt:
        with open(f'model-{epochs}-{learning_rate}.pkl', 'wb+') as output:
            pickle.dump(model, output, pickle.HIGHEST_PROTOCOL)
    with open(f'model-{epochs}-{learning_rate}.pkl', 'wb+') as output:
        pickle.dump(model, output, pickle.HIGHEST_PROTOCOL)
train()