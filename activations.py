import numpy as np

def sigmoid(x):
    return 1.0/(1+np.exp(-x))

def softmax(x):
    return np.exp(x)/(np.sum(np.exp(x)))

def tanh(x):
    return (np.exp(x)-np.exp(-x))/(np.exp(x)+np.exp(-x))

def relu(x):
    x[x < 0] = 0
    return x

def derivative_sigmoid(x):
    return x*(1.0-x)

def derivative_tanh(x):
    return 1 - (tanh(x))**2

def derivative_relu(x):
    return 1. * (x > 0)