import pickle
import numpy as np

def test():
    x_test=np.array([15])
    with open('model-epochs_100000-learning_rate_1e-10.pkl', 'rb') as input:
        model = pickle.load(input)
        np.set_printoptions(precision=10,suppress=True)
        print(model.predict(x_test))
test()